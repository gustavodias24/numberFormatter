from utils.formatarContatos import formatte_number
from utils.compararPlanilhas import compare
from colorama import Fore
from time import sleep
import pandas as pd
import os

path_real = os.path.dirname(os.path.realpath(__file__))

print(Fore.YELLOW + "--- input files ---")
print(Fore.YELLOW + str(os.listdir(f"{path_real}/input")))

confirm = input(Fore.BLUE + "\n\nEnter para confirmar -q para quitar.")

if confirm != "q":
    arquivos = os.listdir(f"{path_real}/input")
    if len(arquivos) == 2:
        for arq in arquivos:
            if ".csv" in arq:
                file_name = str(arq)
            else:
                file_name2 = str(arq)

        formatte_number(file_name, path_real)
        compare(file_name2, file_name, path_real)

        print(Fore.YELLOW + "--- output files ---")
        print(Fore.YELLOW + str(os.listdir(f"{path_real}/output")))
    else:
        print(Fore.RED + "2 arquivos (cvs e um xlsx) é necessário!")


'''
Total_n - 100
Num_Atual - Y

total_n . Y = num_atual . 100

Y = num_atual . 100 / total_n
'''
# print()
# print(os.listdir("input")) os.path.dirname(os.path.realpath(__file__))

# os.remove("input/test.xlsx")

# nu = [1,2,3,3,4]

# for excel in os.listdir("input"):
#     print(excel)
#     numeros = pd.read_excel(f"input/{excel}")["NUMEROS"]
#
#     # begin formatacao
#     formatacao = [n*100 for n in numeros]
#     # end formatacao
#
#     df = pd.DataFrame(formatacao, columns=["NUMEROS"])
#     with pd.ExcelWriter(f"output/formatado-{excel}") as writer:
#         df.to_excel(writer, sheet_name="NUMEROS")
#         os.remove(f"input/{excel}")

# nu = [1,2,3,4,5]
# with pd.ExcelWriter("input/test.xlsx") as writer:
#     df = pd.DataFrame(nu, columns=["NUMEROS"])
#     df.to_excel(writer, sheet_name="NUMEROS")

# Total_n = 89
# for num_atual in range(Total_n):
#     Y = round((num_atual * 100) / Total_n)
#     os.system("clear")
#     print(Fore.RED + f"PROCESSANDO: {Y} %")
#     sleep(0.1)]



