import pandas as pd
import colorama
import os

def compare(file_name, file_normal, path):
    tab_voice = pd.read_excel(f"{path}/input/{file_name}")
    tab_contatos = pd.read_csv(f"{path}/input/contatos-formatados.csv")
    tab_contatos_normal = pd.read_csv(f"{path}/input/{file_normal}")

    numeros = [str(n) for n in tab_contatos["Phone 1 - Value"]]
    numero2 = [str(n) for n in tab_voice["Acesso "]]

    count = 1
    index_count = 0
    list_index = []

    for n in numeros:
        if n in numero2:
            count += 1
            list_index.append(index_count)
        index_count += 1

    Name = [n for n in tab_voice["Usuário "]]
    Phone = [n for n in tab_voice["Acesso "]]
    p = lambda i: {"Name": Name[i], "Phone 1 - Value": Phone[i], "Group Membership": "* myContacts", "Phone 1 - Type": "Mobile"}
    lista_pessoas = [p(i) for i in range(len(Name))]

    df = pd.DataFrame(lista_pessoas, columns=["Name", "Phone 1 - Value", "Group Membership", "Phone 1 - Type"])

    tab_contatos_normal.append(df).drop(index=list_index).to_csv(f"{path}/output/contatos-atualizados.csv", index=False)
    os.system("clear")
    print(colorama.Fore.CYAN + "Limpeza: ok")
    print(colorama.Fore.CYAN + "Formatação: ok")

