import os.path
from colorama import Fore
import pandas as pd

def subs(n):
    new_n = n
    for sym in ["+", "(", ")", "-", ' ', "55"]:
        new_n = str(new_n).replace(sym, '')
    return new_n

def ddd(n):
    if n[0:2] == "91" or n[0:3] == "091":
        if n[0:3] == "091":
            return n[1:]
        return n
    else:
        return f"91{n}"
def formatte_number(file_name, path):
    data = pd.read_csv(f"{path}/input/{file_name}")

    listN = [(ddd(subs(numero))[::-1][0:8]+"919")[::-1] for numero in data["Phone 1 - Value"]]


    i = 0
    for numero in listN:
        data["Phone 1 - Value"][i] = numero
        i += 1

    data.to_csv(f"{path}/input/contatos-formatados.csv")




# with pd.ExcelWriter("output/contatos.xlsx") as writer:
#     df = pd.DataFrame(listN, columns=data.columns)
#     df.to_excel(writer, sheet_name="contatos")

# import pandas as pd
#
# data = pd.read_csv("contatos-formatados.csv")
# for n in data["Phone 1 - Value"]:
#     print(n)
